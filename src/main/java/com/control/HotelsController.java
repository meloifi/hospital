package com.control;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HotelsController
{

    @GetMapping("/getHotels")
    public String getHotels()
    {
        return "HotelDao.findAll()";
    }
}
